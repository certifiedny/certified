At Certified our proficient team ensures success by managing and executing the services of: Project Management, Architectural Glass Wall Distribution & Installation, New Furniture Distribution & Installation, Corporate Relocation, Change Management and Asset Management & Warehousing.

Address: 286 Madison Avenue, 15th Floor, New York, NY 10017, USA

Phone: 212-889-2700
